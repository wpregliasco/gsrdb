from configparser import ConfigParser, ExtendedInterpolation
from os.path import join, exists
import os

## Configuration parameters
cfg = ConfigParser(interpolation=ExtendedInterpolation())
cfg.read('setup.ini')
DB   = cfg['MySQL']['DBname']
user = cfg['MySQL']['username']

## post operations
cmd = f'mysql -u {user} {DB} < post.sql'
os.system(cmd)

print('done.')