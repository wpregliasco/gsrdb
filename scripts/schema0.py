from configparser import ConfigParser, ExtendedInterpolation
from os.path import exists
import os


def loadPaths(cfg):
    '''loads Paths variables from ConfigParser instance

    '''
    def pathError(_path):
        print(f'Error in setup.ini: {_path} do not exists')
        exit(0)

    # Import and Check paths
    baseDir   = cfg['PATHS']['baseDir']
    if not exists(baseDir):
        pathError('baseDir')

    dataDir   = cfg['PATHS']['dataDir'] 
    if not exists(dataDir):
        pathError('dataDir')

    schemaDir = cfg['PATHS']['schemaDir'] 
    if not exists(schemaDir):
        pathError('schemaDir')

    oneData   = cfg['PATHS']['oneData']
    if not exists(oneData):
        pathError('oneData')

    return baseDir, dataDir, schemaDir, oneData

def parse_tables(fname):
    '''loads a schema in a structure

       table_keys: list of table names
       table_dict: dict of tables [col_keys,col_dict]
       col_keys:   list of columns in table
       col_dict:   dict of column types

       the reverse operation is in function `save_tables`
    '''

    with open(fname,'r') as f:
        table_keys  = []
        table_dict   = {}
        readingTable = False

        for line in f:
            if not readingTable:
                if line.strip().startswith('CREATE TABLE'):
                    # open table
                    table_keys.append(line.split("`")[1])
                    col_keys = []
                    col_dict  = {}
                    readingTable = True

            else: 
                if line.strip() == ");":
                    # close table
                    table_dict[table_keys[-1]] = [col_keys, col_dict]
                    readingTable = False
                elif "`" in line.strip():
                    # new column
                    k,v = line.strip().split("`")[1:]
                    col_keys.append(k)
                    col_dict[k] = v.strip().replace(',','')

        return [table_keys, table_dict]

def save_tables(table_keys, table_dict, fname=None):
    '''save the structure in a mysql schema

       This is the inverse operation of `parse_tables`
    '''

    out = ''
    for tk in table_keys:
        out += f'CREATE TABLE `{tk}`\n (\n'
        
        ks, d = table_dict[tk]
        ends = ',\n'
        for k in ks:
            if k == ks[-1]:
                ends = '\n'

            out += f"\t`{k}`\t\t{d[k]}{ends}"

        out += ');\n\n'

    if fname is not None:
        with open(fname,'w') as f:
            f.write(out)
    return out

def tweak_tables(table_keys, table_dict):
    '''Minor ad-hoc fixes in column schema

    '''

    table_dict['GlobalVariables'][1]['Definition'] = 'text (1024)'
    table_dict['Areas'][1]['Definition'] = 'text (1024)'
    table_dict['Measurements'][1]['Units'] = 'varchar (20)'

    return [table_keys, table_dict]

def main():

    ## setup.ini 
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read('setup.ini')

    baseDir, dataDir, schemaDir, oneData = loadPaths(cfg)

    ## schema0
    schema0 = f"{schemaDir}/schema0.sql"
    cmd = f'mdb-schema "{oneData}" > "{schema0}" mysql'
    os.system(cmd)
    print('Command: {cmd}')
    print('schema0.sql: saved.')

    ## read/tweak/save schema1
    Tables = parse_tables(schema0)
    Tables = tweak_tables(*Tables)
    schema1 = f"{schemaDir}/schema1.sql"
    save_tables(*Tables, schema1)
    print('schema1.sql: saved.')

    ## list of tables
    tables_file = f"{schemaDir}/tables.dat"
    with open(tables_file,'w') as f:
        for item in Tables[0]:
            f.write(item)
            if item != Tables[0][-1]:
                f.write('\n')
    print('tables.dat: saved.')

    print('done')

if __name__ == "__main__":
   main()