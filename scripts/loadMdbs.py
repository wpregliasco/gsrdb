from configparser import ConfigParser, ExtendedInterpolation
from os.path import exists
from pathlib import Path
import os
import datetime as dt
import subprocess
import stat

DIRLIST = False

def loadPaths(cfg):
    '''loads Paths variables from ConfigParser instance

    '''
    def pathError(_path):
        print(f'Error in setup.ini: {_path} do not exists')
        exit(0)

    # Import and Check paths
    baseDir   = cfg['PATHS']['baseDir']
    if not exists(baseDir):
        pathError('baseDir')

    dataDir   = cfg['PATHS']['dataDir'] 
    if not exists(dataDir):
        pathError('dataDir')

    schemaDir = cfg['PATHS']['schemaDir'] 
    if not exists(schemaDir):
        pathError('schemaDir')

    oneData   = cfg['PATHS']['oneData']
    if not exists(oneData):
        pathError('oneData')

    return baseDir, dataDir, schemaDir, oneData

def check_fname(path_name):
    '''Returns True if is not a silly filename

       Tuneado para los archivos de Rosario
    '''
    if len(path_name.strip())== 0:
        return False

    fname = path_name.split('/')[-1]
    if 'copy' in fname.lower():
        return False
    return True

def load_tables_list(fname):
    with open(fname, 'r') as f:
        out = f.read()
    out = out.split('\n')
    out = list(filter(lambda x: len(x.strip())>0, out))
    return out

def validDate(date_str):
    try:
        date = dt.datetime.strptime(date_str,'%Y-%m-%d')
    except:
        return False

    if date> dt.datetime(2000,1,1) and date < dt.datetime.now():
        return True
    return False

def guess_date(mdb, s_dir, path):
    # prueba 1: fecha separada por guiones en el directorio
    date = False
    if len(s_dir)>9:
        if s_dir[4]=='-' and s_dir[7]=='-':
            date_ = s_dir[:10]
            if validDate(date_):
                date = date_
                try:
                    extra= s_dir[10:]
                except:
                    extra = ''
    # prueba2: fecha sin separación
    if not date:
        date_ = '        '
        # largo correcto
        try:
            date_ = s_dir[:8]
        except:
            date_ = False
        if date_:
            #fecha correcta
            Y = date_[:4]
            M = date_[4:6]
            D = date_[6:8]
            date_ = f'{Y}-{M}-{D}'
            if validDate(date_):
                date = date_
                try:
                    extra= s_dir[8:]
                except:
                    extra = ''
    # prueba 3: fecha del archivo
    if not date:
        fname = Path(mdb)
        dt_date = dt.datetime.fromtimestamp(fname.stat().st_mtime)
        date_ = dt_date.strftime('%Y-%m-%d')
        if validDate(date_):
            date  = date_
            extra = path[:-len(path.split('.')[-1])-1]
    # en caso de que todo salga mal
    if not date:
        date = '2001-12-20'
        extra= 'helicopter'

    if extra[0] in '-._':
        extra = extra[1:]
    if extra.endswith('_data'):
        extra = extra[:-len('_data')]
    extra = extra.replace(' ','')

    return date, extra

def load_Files_sql(i,path,extra,date):
    out =  'INSERT INTO `Files` (`FileID`, `Path`, `Extra`, `Date`) '
    out+= f'VALUES ({i},"{path}","{extra}","{date}");\n'
    return out

def add_fileID(sql, id):
    # filter recycled
    out = ''
    for line in sql.split('\n'):
        if line.strip()=='':
            continue
        assert line.startswith('INSERT INTO `')
        
        #### FieldID
        ## Apertura de paréntesis
        i = line.find('(',len('INSERT INTO `'))
        ## Segundo campo
        i = line.find(', ',i+1) + 2
        ## Insert FileID
        line = line[:i] + '`FileID`, ' + line[i:]
        
        #### Value
        ## Cierre del paréntesis
        i = line.find(')', i+len('`FileID`, ')) + 2
        ## VALUES
        assert line[i:].startswith('VALUES (')
        i += len('VALUES (')
        ## Segundo valor
        i = line.find(',', i) +1
        ## Insert Value
        endline = line[i:]
        try:
           endline.encode('utf8').decode('cp1252')
        except:
           j = endline.find(',')
           if j==-1: j=endline.find(')')
           endline='"---"'+endline[j:]

        line = line[:i] + str(id) + ',' + endline

        out += line + '\n'

    return out[:-1]

def load_tables_sql(filepath, t, i):
    mdb_ = r'{}'.format(filepath)
    cmd = ['mdb-export', '-I', 'mysql', mdb_, t]
    out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
    out = add_fileID(out, i)
    return out + '\n'

def map_tables_sql(sql, name_in, name_new):
    out = sql.replace(f"`{name_in}`",f"`{name_new}`")
    return out

def load_mdb(DB, user, mdb, dataDir, i, temp, tables):

    ## para cargar la tabla Files, necesito:
    #  i:      ordinal (entra como argumento)
    #  path:   pathname completo sin la base dataDir
    #  extra:  es lo que está en el path y excede la fecha
    #  date:   YYY-MM-DD
    L = len(dataDir)+1
    fname = mdb.split('/')[-1]
    s_dir = mdb[L:-len(fname)-1]
    sep = '/'
    if s_dir=='':
        sep=''
    path  = f'{s_dir}{sep}{fname}'    # pathfile relativo a dataDir
    print(f' ID:{i:>6} {path}',end='')

    # guess de fecha
    date, extra = guess_date(mdb, s_dir, path)

    # archivo temporario de salida (temp)
    header='''
           SET autocommit=0;
           SET unique_checks=0;
           SET foreign_key_checks=0;\n\n'''
    footer='''
           COMMIT;
           SET unique_checks=1;
           SET foreign_key_checks=1;'''
    
    with open(temp,'w') as f:
        f.write(header)

        # tabla Files
        sql = load_Files_sql(i,path,extra,date)

        f.write(sql)

        # resto de las tablas
        for t in tables:
            sql = load_tables_sql(mdb, t, i)
            sql = map_tables_sql(sql, 'Rank', 'Rank_')

            f.write(sql)
    
        f.write(footer)

    # carga temp en MySQL
    #Import Common Tables
    cmd = f'mysql -u {user} {DB} < "{temp}"'
    os.system(cmd)

    print(' .-')

def isValid(DB, user, mdb, sqlDir):

    sql = f'{sqlDir}/valid.sql'

    sql_ = r'{}'.format(sql)
    mdb_ = r'"{}"'.format(mdb)

    cmd = ['mdb-sql', '-HFp','-d','','-i', sql_, mdb_]

    out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')

    if len(out.split('\n'))==2:
        return False

    return True

def max_file_id(DB, user, sqlDir):

    sql = '"SELECT MAX(FileID) FROM Files"'
    cmd = '#!/bin/sh\n'
    cmd+= f'echo {sql} | mysql -N -u{user} {DB}'

    ## make executable script
    tmp = f'{sqlDir}/tmpcmd.sh'
    tmp = r'{}'.format(tmp)
    with open(tmp,'w') as f:
        f.write(cmd)

    ## make it executable
    st = os.stat(tmp)
    os.chmod(tmp, st.st_mode | stat.S_IEXEC)

    mode = os.stat(tmp).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(tmp, mode)

    ## run
    out = subprocess.run([tmp], stdout=subprocess.PIPE).stdout.decode('utf-8')

    ## remove temp file    
    try:
        os.remove(tmp)
    except OSError:
        pass
    

    if len(out)==0:
        print(f'Error: database {DB} is not present.')
        exit()
    
    out = out[:-1]
    if 'NULL' in out:
        return 0
    
    return int(out)

def read_MySQL_files(DB, user):

    sql = '"SELECT Path FROM Files"'
    cmd = '#!/bin/sh\n'
    cmd+= f'echo {sql} | mysql -N -u{user} {DB}'

    ## make executable script
    tmp = f'tmpcmd.sh'
    tmp = r'./{}'.format(tmp)
    with open(tmp,'w') as f:
        f.write(cmd)

    ## make it executable
    st = os.stat(tmp)
    os.chmod(tmp, st.st_mode | stat.S_IEXEC)

    mode = os.stat(tmp).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(tmp, mode)

    ## run
    out = subprocess.run([tmp], stdout=subprocess.PIPE).stdout.decode('utf-8')

    ## remove temp file    
    try:
        os.remove(tmp)
    except OSError:
        pass

    if len(out)==0:
        return []
    
    out = out.split('\n')

    if 'NULL' in out:
        return []
    
    out=list(filter(lambda x: len(x.strip())!=0, out))

    return out

def main():

    ## setup.ini 
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read('setup.ini')

    baseDir, dataDir, schemaDir, oneData = loadPaths(cfg)

    ## check dirlist (if not done, exits)
    dirlist = f'{dataDir}/dirList.dat'
    if not exists(dirlist) or DIRLIST: 
        ## read directories
        with open(dirlist,'w') as f:
            for path in Path(dataDir).rglob('**/*.mdb'):
                f.write(str(path)+'\n')
        print('dirList saved. Check and rerun to further processing.')
        return 'dirList'

    ## load files
    files = []
    with open(dirlist,'r') as f:
        for name in f:
            name = name.strip()
            if name=='':
                continue
            if '#' in name:
                name = name.split('#')[0].strip()
            if name[-1]=='\n':
                name = name[:-1]
            if check_fname(name):
                files.append(name)

    ## listado de tablas
    tables_sample = f"{schemaDir}/tables_sample.dat"
    sample = load_tables_list(tables_sample)

    ## load mdbs
    DBname   = cfg['MySQL']['DBname']
    username = cfg['MySQL']['username']

    ## listado en MySQL
    loaded_files = read_MySQL_files(DBname, username)
    loaded_files = list(map(lambda x: f'{dataDir}/{x}', loaded_files))

    ## files to load
    files_to_load = list(filter(lambda x: x not in loaded_files, files))

    tempFile = f'{schemaDir}/tempTables.sql'

    inValid = []
    i0 = max_file_id(DBname, username, schemaDir)
    id = i0+1
    N=len(files_to_load)
    for i, fil in enumerate(files_to_load, start=1):  #siempre empiezo por 1 i: indice del file en files
        if isValid(DBname, username, fil, schemaDir):
            print(f'#{i}/{N}',end=' - ')
            load_mdb(DBname, username, fil, dataDir, id, tempFile, sample)
            id+=1
        else:
            inValid.append(i) 

    ## annotate inValid files
    with open(dirlist,'w') as f:
        for i,fi in enumerate(files, start=1):
            out = fi
            if i in inValid:
                out += '          # inValid file'
            
            out += '\n'

            f.write(out)

    ## remove temp file    
    try:
        os.remove(tempFile)
    except OSError:
        pass


    print('done.')
    return 'all'

if __name__ == "__main__":
   main()