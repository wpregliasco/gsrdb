#################################################################
# AreaID ##################################################

UPDATE Files f
JOIN (
        SELECT s.FileID, s.AreaID, s.nfields
        FROM (
                SELECT FileID, 
                    AreaID, 
                    COUNT(FieldID) AS nfields 
                FROM Fields 
                GROUP BY FileID, AreaID
            ) AS s
        WHERE s.nfields>1
     ) AS so
     ON so.FileID=f.FileID
SET f.AreaID = so.AreaID
WHERE f.AreaID IS NULL;

#################################################################
# Measurements ##################################################

## Fill uMeasurements
INSERT INTO uMeasurements
    ( Type,
      Definition,
      Units,
      Name     
    )
    SELECT DISTINCT 
        Type,
        Definition,
        Units,
        Name 
    FROM Measurements M
    WHERE (Type!=402 
           AND M.Definition NOT IN (SELECT uM.Definition 
                                    FROM   uMeasurements AS uM 
                                    WHERE  Type!=402
                                   )
          )
    ORDER BY Type,Definition;

## Create uResults
CREATE TABLE IF NOT EXISTS uResults (
    FileID         INT NOT NULL,
    FeatureID      INT NOT NULL, 
    uMeasurementID INT NOT NULL, 
    Value          FLOAT NOT NULL, 
    Error          FLOAT NOT NULL
    ); 

## Load uResults
INSERT INTO uResults ( 
    FileID,
    FeatureID, 
    uMeasurementID, 
    Value, 
    Error
    )
    SELECT
        r.FileID,
        r.FeatureID, 
        u.uMeasurementID, 
        r.Value, 
        r.Error
    FROM Results AS r
         JOIN (Measurements AS m, uMeasurements AS u)
              ON (r.MeasurementID=m.MeasurementID AND
                  r.FileID = m.FileID AND
                  m.Definition=u.Definition
                 );

## Empty tables
TRUNCATE TABLE Measurements;
TRUNCATE TABLE Results;