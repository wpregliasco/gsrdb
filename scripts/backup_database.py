from configparser import ConfigParser, ExtendedInterpolation
import os
from os.path import exists
from pathlib import Path
from datetime import datetime as dt

'''Hace un backup de la base de datos 
   lee DB que está indicada en setup.ini
   salva el archivo en el directorio `dataDir`

   El formato del archivo es:
        Backup_nn_yyyy-mm-dd.sql|.zip

    nn        es un número de índice
    yy-mm-dd  es la fecha
'''

def next_backup(dataDir):
    paths = Path(dataDir).rglob('Backup_??_*.*')
    paths = list(map(lambda x: str(x)[len(dataDir)+1:], paths))
    files = list(filter(lambda x: x.split('.')[-1] in ['sql','zip'], paths))

    files = sorted(files, key = lambda x: x[len('Backup_'):len('Backup_')+2] )
    try:
        nn = files[-1][len('Backup_'):len('Backup_')+2]
    except:
        nn ='0'
        
    nn = int(nn)+1

    out = f'{dataDir}/Backup_{nn:02}_{dateStr()}.sql'
    return out

def dateStr():
    return dt.now().strftime('%Y-%m-%d')

## setup.ini 
cfg = ConfigParser(interpolation=ExtendedInterpolation())
cfg.read('setup.ini')

#  dataDir
dataDir   = cfg['PATHS']['dataDir'] 
if not exists(dataDir):
    print(f'Error in setup.ini: {dataDir} do not exists')

#  DB data
DB   = cfg['MySQL']['DBname']
user = cfg['MySQL']['username']

## Archivo de salida
ou_file = next_backup(dataDir)
basename = '.'.join(ou_file.split('.')[:-1])

## backup
print(f'exporting {basename} ...', end='')
cmd = f'''mysqldump -u{user} {DB} \
          --single-transaction \
          --quick \
          --lock-tables=false > "{ou_file}"'''
os.system(cmd)
print('done.')

## zip
print(f'zipping ...', end='')
cmd = f'zip -j {basename}.zip {basename}.sql'
os.system(cmd)

## borro
try:
    os.remove(ou_file)
except OSError:
    pass

print('done.')


