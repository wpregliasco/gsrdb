from configparser import ConfigParser, ExtendedInterpolation
from os.path import join, exists
import os
from elements import ELEMENTS


def loadPaths(cfg):
    '''loads Paths variables from ConfigParser instance

    '''
    def pathError(_path):
        print(f'Error in setup.ini: {_path} do not exists')
        exit(0)

    # Import and Check paths
    baseDir   = cfg['PATHS']['baseDir']
    if not exists(baseDir):
        pathError('baseDir')

    dataDir   = cfg['PATHS']['dataDir'] 
    if not exists(dataDir):
        pathError('dataDir')

    schemaDir = cfg['PATHS']['schemaDir'] 
    if not exists(schemaDir):
        pathError('schemaDir')

    oneData   = cfg['PATHS']['oneData']
    if not exists(oneData):
        pathError('oneData')

    return baseDir, dataDir, schemaDir, oneData

def parse_tables(fname):
    '''loads a schema in a structure

       table_keys: list of table names
       table_dict: dict of tables [col_keys,col_dict]
       col_keys:   list of columns in table
       col_dict:   dict of column types

       the reverse operation is in function `save_tables`
    '''

    with open(fname,'r') as f:
        table_keys  = []
        table_dict   = {}
        readingTable = False

        for line in f:
            if not readingTable:
                if line.strip().startswith('CREATE TABLE'):
                    # open table
                    table_keys.append(line.split("`")[1])
                    col_keys = []
                    col_dict  = {}
                    readingTable = True

            else: 
                if line.strip() == ");":
                    # close table
                    table_dict[table_keys[-1]] = [col_keys, col_dict]
                    readingTable = False
                elif "`" in line.strip():
                    # new column
                    k,v = line.strip().split("`")[1:]
                    col_keys.append(k)
                    col_dict[k] = v.strip().replace(',','')

        return [table_keys, table_dict]

def save_tables(table_keys, table_dict, fname=None):
    '''save the structure in a mysql schema

       This is the inverse operation of `parse_tables`
    '''

    out = ''
    for tk in table_keys:
        out += f'CREATE TABLE `{tk}`\n (\n'
        
        ks, d = table_dict[tk]
        ends = ',\n'
        for k in ks:
            if k == ks[-1]:
                ends = '\n'

            out += f"\t`{k}`\t\t{d[k]}{ends}"

        out += ');\n\n'

    if fname is not None:
        with open(fname,'w') as f:
            f.write(out)
    return out

def load_tables_list(fname):
    with open(fname, 'r') as f:
        out = f.read()
    out = out.split('\n')
    out = list(filter(lambda x: len(x.strip())>0, out))
    return out

def save_tables_common_sql(fname, common, oneData):
    header='''
           SET autocommit=0;
           SET unique_checks=0;
           SET foreign_key_checks=0;\n'''
    template_cmd = 'mdb-export -I mysql "{}" {} >> "{}"'
    footer='''
           COMMIT;
           SET unique_checks=1;
           SET foreign_key_checks=1;'''

    with open(fname,'w') as f:
        f.write(header)
    for t in common:
        cmd = template_cmd.format(oneData, t, fname)
        os.system(cmd)
    with open(fname,'a') as f:
        f.write(footer)

def load_to_mysql(DB, user, schema, tables):
    # Delete database if exists
    cmd = f'mysql -u {user} -e "drop database if exists {DB};"'
    os.system(cmd)

    #Create the database
    cmd = f'mysql -u {user} -e "create database {DB};"'
    os.system(cmd)

    #Import schema
    cmd = f'mysql -u {user} {DB} < "{schema}"'
    os.system(cmd)

    #Import Common Tables
    cmd = f'mysql -u {user} {DB} < "{tables}"'
    os.system(cmd)

def map_schema_column(table_keys, table_dict, name_in, name_new):
    for t in table_keys:
        column_keys, column_dict = table_dict[t]
        if name_in in column_keys:
            # replace in keys
            i = column_keys.index(name_in)
            column_keys[i] = name_new
            # replace in dict
            column_dict[name_new]=column_dict[name_in]

    return [table_keys, table_dict]

def map_tables_sql(sql, name_in, name_new):
    out = sql.replace(f"`{name_in}`",f"`{name_new}`")
    return out

def load_elements(cfg):
    ''' Carga los elementos en uMeasurements
    '''
    ## Configuration parameters
    DB   = cfg['MySQL']['DBname']
    user = cfg['MySQL']['username']

    for e in ELEMENTS:
        uMs1 = f"402, 'AC% {e.number}', 'AC%', '{e.symbol}'"
        uMs2 = f"402, 'Wt% {e.number}', 'Wt%', '{e.symbol}'"

        SQL = f"INSERT INTO uMeasurements (Type, Definition, Units, Name) VALUES ({uMs1});"
        cmd =  f'mysql -u {user} {DB} -e "{SQL}"'
        os.system(cmd)

        SQL = f"INSERT INTO uMeasurements (Type, Definition, Units, Name) VALUES ({uMs2});"
        cmd =  f'mysql -u {user} {DB} -e "{SQL}"'
        os.system(cmd)

def main():
    ## setup.ini 
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read('setup.ini')

    baseDir, dataDir, schemaDir, oneData = loadPaths(cfg)

    ## listados de tablas
    tables_common = f"{schemaDir}/tables_common.dat"
    common = load_tables_list(tables_common)

    tables_sample = f"{schemaDir}/tables_sample.dat"
    sample = load_tables_list(tables_sample)

    ## schema1: load 
    schema1 = f"{schemaDir}/schema1.sql"
    Tables = parse_tables(schema1)

    ## clean not listed tables
    for table_key in Tables[0][::-1]:
        if table_key not in common+sample:
            Tables[0].remove(table_key)
            
    ## add table: Files
    Tables[0] = ['Files'] + Tables[0]
    Tables[1]['Files'] = [[],{}]
    Tables[1]['Files'][0] = ['FileID','Path','Extra','Date','AreaID']
    Tables[1]['Files'][1]['FileID'] = 'int'
    Tables[1]['Files'][1]['Path']   = 'varchar (64) NOT NULL'
    Tables[1]['Files'][1]['Extra']  = 'varchar (32)'
    Tables[1]['Files'][1]['Date']   = 'DATE'
    Tables[1]['Files'][1]['AreaID']   = 'VARCHAR(64)'

    ## add FileID to sample tables
    for table_key in Tables[0]:
        if table_key in sample:
            Tables[1][table_key][0] = ['FileID'] + Tables[1][table_key][0]
            Tables[1][table_key][1]['FileID'] = 'int NOT NULL'
    
    ## filter Rank reserved word
    Tables = map_schema_column(*Tables, 'Rank', 'Rank_')

    ## create uTables
    Tables[0].append('uMeasurements')
    Tables[1]['uMeasurements'] = [[],{}]
    Tables[1]['uMeasurements'][0] = ['uMeasurementID','Type','Definition','Units', 'Name']
    Tables[1]['uMeasurements'][1]['uMeasurementID'] = 'INT AUTO_INCREMENT PRIMARY KEY'
    Tables[1]['uMeasurements'][1]['Type']           = 'INT'
    Tables[1]['uMeasurements'][1]['Definition']     = 'VARCHAR (255) NOT NULL UNIQUE'
    Tables[1]['uMeasurements'][1]['Units']          = 'VARCHAR (20)'
    Tables[1]['uMeasurements'][1]['Name']           = 'VARCHAR (64)'

    Tables[0].append('uResults')
    Tables[1]['uResults'] = [[],{}]
    Tables[1]['uResults'][0] = ['FileID', 'FeatureID', 'uMeasurementID','Value', 'Error']
    Tables[1]['uResults'][1]['FileID']             = 'INT NOT NULL'
    Tables[1]['uResults'][1]['FeatureID']          = 'INT NOT NULL' 
    Tables[1]['uResults'][1]['uMeasurementID']     = 'INT NOT NULL' 
    Tables[1]['uResults'][1]['Value']              = 'FLOAT NOT NULL'
    Tables[1]['uResults'][1]['Error']              = 'FLOAT NOT NULL'

    ## schema2: save
    schema2 = f"{schemaDir}/schema2.sql"
    save_tables(*Tables, schema2)
    print('schema2.sql: saved.')

    ## tables_common.sql
    tables_common_sql =  f"{schemaDir}/tables_common.sql"
    save_tables_common_sql(tables_common_sql, common, oneData)
    #...replace column names
    with open(tables_common_sql,'r') as f:
        tables = f.read()

    tables = map_tables_sql(tables, 'Rank', 'Rank_')

    with open(tables_common_sql,'w') as f:
        f.write(tables)

    print('tables_common.sql: saved.')

    ## Generate DB, load schema and tables_common
    DBname   = cfg['MySQL']['DBname']
    username = cfg['MySQL']['username']
    print('loading to MySQL...',end='')
    load_to_mysql(DBname, username, schema2, tables_common_sql)
    print('loaded.')

    ## Populate uMeasurements with periodicTable
    load_elements(cfg)
    print('elements loaded.')

    print('done.')

if __name__ == "__main__":
   main()
