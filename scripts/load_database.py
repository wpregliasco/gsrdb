from configparser import ConfigParser, ExtendedInterpolation
import os
from os.path import exists
from pathlib import Path
from datetime import datetime as dt

'''Carga una base de datos en MySQL
   lee DB del directorio `dataDir` que está indicado en setup.ini

   El formato del archivo es:
        Backup_nn_yyyy-mm-dd.sql

    nn        es un número de índice
    yy-mm-dd  es la fecha

    Lee el archivo .sql con el nn más grande
'''

def file_backup(dataDir):
    paths = Path(dataDir).rglob('Backup_??_*.sql')
    files = list(map(lambda x: str(x)[len(dataDir)+1:], paths))
    files = sorted(files, key = lambda x: x[len('Backup_'):len('Backup_')+2] )

    # no file
    if len(files)==0:
        print(f'Error: no backup file in {dataDir}.')
        exit()

    #out = files[-1]
    out = f'{dataDir}/{files[-1]}'
    return out

## setup.ini 
cfg = ConfigParser(interpolation=ExtendedInterpolation())
cfg.read('setup.ini')

#  dataDir
dataDir   = cfg['PATHS']['dataDir'] 
if not exists(dataDir):
    print(f'Error in setup.ini: {dataDir} do not exists')

#  DB data
DB   = cfg['MySQL']['DBname']
user = cfg['MySQL']['username']

## Archivo de entrada
infile = file_backup(dataDir)

## Regenero la base de datos
cmd  = f'echo "DROP DATABASE IF EXISTS {DB};" | mysql -u{user}'
os.system(cmd)
cmd  = f'echo "CREATE DATABASE {DB};" | mysql -u{user}'
os.system(cmd)


## Cargo en MySQL
cmd = f'mysql -u{user} {DB} < "{infile}"'
os.system(cmd)

print('done.')