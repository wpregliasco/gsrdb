'''qload  

   Carga rápida de .mdb a bas de datos MySQL
   No pregunta nada, no hay nada customizado, 
   pero hace las operaciones habituales y debería andat bien. 
'''

# schema1.py
import schema1

print('-'*70)
print(('##### schema1 '+'#'*70)[:70])
print('-'*70)
schema1.main()

# loadMdbs.py
import loadMdbs

print('-'*70)
print(('##### loadMdbs '+'#'*70)[:70])
print('-'*70)
loadMdbs.DIRLIST = True
res = loadMdbs.main()

if res == 'dirList':
    print()
    loadMdbs.DIRLIST = False
    loadMdbs.main()

# post.py
print('-'*70)
print(('##### post '+'#'*70)[:70])
print('-'*70)

import post

# backup_database.py
print('-'*70)
print(('##### backup_database '+'#'*70)[:70])
print('-'*70)

import backup_database

# bye
print('-'*70)
