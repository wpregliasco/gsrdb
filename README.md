# gsrDB

Interfaz para leer las bases de datos de residuos de disparo (GSR) de los microscopios de barrido. 

Los microscopios que se utilizan en pericias, están provistos de un sistema automático de análisis de GSR, que es un software privativo de la empresa _OXFORD_: [INCA Energy - Software Interface](https://www.brandonu.ca/microscope/files/2010/08/pdf_3.pdf).

El software es muy útil para analizar las muestras de una en una, pero carece de funcionalidades para hacer estadísticas sobre un conjunto grande de muestras. 

Estos sistemas generan muchos archivos ``.mdb`` que son bases de datos diseñadas por _MicroSoft_ (hechas para *Microsoft Access*).

Nuestros programas leen estos archivos y los incorporan a una base de datos común en formato *MySQL* desde donde pueden ser procesadas con una interfaz en Python.

Para instalación y uso, [consultar la documentación](https://wpregliasco.gitlab.io/gsrdb).

## Instalación

La instalación tiene sus detalles que se pueden consultar en el [documento de Instalación](https://wpregliasco.gitlab.io/gsrdb/instalacion.html).
Se pueden resumir los pasos en:

* instalar el Python via Anaconda 
* instalar la base de datos Mysql con un usuario sin password
* instalar el git

Nos ubicamos en un directorio de proyectos adonde queremos instalar este programa. 

``` bash
sudo apt install mdbtools
git clone https://gitlab.com/wpregliasco/gsrdb.git
cd gsrdb
conda deactivate
conda update -n base -c defaults conda
conda env remove --name gsr
conda env create -f environment.yml
conda activate gsr
pip install -e .
```

## Cargar Base de Datos MySQL

Nos paramos en el directorio `scripts` del proyecto 
y activamos el entorno de Python con el comando: `conda activate gsr`

Hay que editar el archivo `setup.ini` con los directorios del caso. 

### Si no tenemos hecha la base de datos MySQL

Corriendo el programa

``` bash
(gsr) $ python qload.py
```
se cargarán todos los archivos que se encuentran en los subdirectorios de Datos
a la base MySQL. Este proceso puede ser lento y se recomienda lanzarlo en una buena 
máquina. El resultado será un archivo de backup que puede ser importado en otro ambiente.

### Si contamos con un archivo de backup 

Colocamos ese archivo en el directorio de datos. 
Si está comprimido, lo descomprimimos y corremos el comando

``` bash
(gsr) $ python load_database.py
```

## Análisis de Datos

_Lorem ipsum_