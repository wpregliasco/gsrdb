import mysql.connector
from mysql.connector import Error
import xml.etree.ElementTree as ET
from xml.dom import minidom
from numpy import sqrt, pi, array
from matplotlib.pyplot import *

def dist(r1,r2):
    '''Distancia entre dos puntos
    '''
    x1,y1 = r1
    x2,y2 = r2
    return sqrt((x2-x1)**2+(y2-y1)**2)

#------------------------------------------------------------------------
class MySQL:
    Host = 'localhost'
    DB   = 'gsr0'
    User = 'willy'
    Pass = 'zapandanga'
    verb = False
    
    ####################################################################
    def __init__(self):
        '''Open connection and cursor
        '''
        self.Connection = None
        self.Cursor     = None
        self.open()
        
    def open(self):
        '''Definida aparte para usar con with...
        '''
        try:
            self.Connection = mysql.connector.connect(host    =self.Host,
                                                      database=self.DB,
                                                      user    =self.User,
                                                      password=self.Pass)
            self.Cursor = self.Connection.cursor()
            if self.verb:
                print("Database {} connected".format(self.DB))

        except  Error as e:
            print ("Error while connecting to MySQL", e)
            
    def close(self):
        '''Esta función cierra la conexón.
        '''
        if(self.Connection.is_connected()):
            self.Cursor.close()
            self.Connection.close()
            if self.verb:
                print("MySQL connection is closed")
        else:
                print("MySQL connection already closed")
                
    ####################################################################
    def query(self,*QQ):
        '''Query
           Tiene tres modos de funcionamiento:
           * una entrada     (Q)      : query (Q)
           * dos entradas    (Q,W)    : query (Q WHERE W)
           * tres entradas   (Q,T,W)  : query (Q FROM T WHERE W)
           * cuatro entradas (Q,T,W,G): query (Q FROM T WHERE W GROUP BY G)
           Si se omite el SELECT, lo agrega.
           
           La salida elimina la lista su hay un sólo resultado.
           Elimina las listas de tuplas de sólo un elemento.
        '''
        if len(QQ)==4:
            Q,T,W,G = QQ
            Q = f'{Q.strip()} FROM {T.strip()} WHERE ({W.strip()}) GROUP BY {G.strip()};'
        elif len(QQ)==3:
            Q,T,W = QQ
            Q = f'{Q.strip()} FROM {T.strip()} WHERE ({W.strip()});'
        elif len(QQ)==2:
            Q,W = QQ
            Q = f'{Q.strip()} WHERE ({W.strip()});'
        elif len(QQ)>4:
            raise Exception('Error in query: 3 arguments or less expected.')
        else:
            Q = QQ[0]
        
        if (not Q.lower().startswith('select')) and \
           (not Q.lower().startswith('show')):    
            Q = 'SELECT ' + Q.strip()
            
           
        self.Cursor.execute(Q)
        R = self.Cursor.fetchall()
        if R==[]:
            return []
        if len(R[0])==1:
            R = list(map(lambda x: x[0],R))
        if len(R)==1:
            R=R[0]
        
        return R        
        
    def columns(self, table):
        '''columns
           Da una lista de los nombres de las columnas de la tabla 'table'
        '''
        cols = self.query(f'SHOW COLUMNS FROM {table}')
        return list(map(lambda x:x[0],cols))

#------------------------------------------------------------------------    
class Xml:
    '''et
       ElementTree parser de xml
       xmlStr: string en formato texto xml. 
       devuelve un objeto ElementTree
       Ejemplo: xml
           <main Shape=0>
               <d0 x=1, y=2 />
               <d1 x=3, y=5 />
           </main>
           --------------------------
           out.tag    : 'main' 
           out.attrib : {'Shape':0}
           out[i]     : Element di
    '''
    def __init__(self,xmlStr):
        self.e   = ET.fromstring(xmlStr)
        self.str = xmlStr
        return
    
    def ps(self):
        '''Preety String from xml
           Devuelve un string identado muy bonito
        '''
        rootstr = minidom.parseString(self.str).toprettyxml(indent="  ")
        rootstr = rootstr[rootstr.find('\n')+1:]
        return rootstr
    
#------------------------------------------------------------------------
class Diver:
    ####################################################################
    ############################## Carga e inicialización
    def __init__(self, FileID, loadFeatures=True, loadResults=True):
        ## Inicializa el acceso a la Base de datos
        self.DB=MySQL()
        
        ## Datos de la tabla File
        Q = 'FileID, Path, Extra, Date, Valid, AreaID'
        R=self.DB.query(Q,'Files', f'FileID={FileID}')
        self.FileID = R[0]
        self.ID = self.FileID
        self.Path   = R[1]
        self.Extra  = R[2]
        self.Date   = R[3]
        self.Valid  = R[4]
        self.AreaID = R[5]
        
        ## Carga Areas
        self.loadAreas()
        
        if loadFeatures:
           ## Carga Features
           self.loadFeatures()
           
           if loadResults:
              ## Carga Results
              self.loadResults()
        ## Chau
        self.close()
        
    def close(self):
        '''Cierra el acceso a la base de datos.
           No altera los objetos cargados en memoria pero se pierde la conectividad.
        '''
        self.DB.close()
        
    ####################################################################
    ############################## Loaders
    def loadAreas(self):
        ### Cargo todo
        Areas = []
        for row in self.DB.query('* FROM Areas',f'FileID={self.FileID}'):
            Areas.append(Area(row))
        
        ### Número de Fields
        Q = 'AreaID, count(*)'
        F = 'Fields'
        W = f'FileID={self.FileID}'
        G = 'AreaID'
        fdata = self.DB.query(Q,F,W,G)
        
        if fdata !=[]:
            if isinstance(fdata, tuple):
                fdata = [fdata]
            aID,Fields = zip(*fdata)
        else: 
            aID,Fields = [],[]
        ### Clasifico Áreas
        self.Area0        = None
        self.Areas_Checks = []
        self.Areas_Meas   = []
        self.Areas_Xtras  = []
        if self.Valid==1:
            self.Area0        = list(filter(lambda a: a.ID==self.AreaID, Areas))[0]
            self.Area0.Fields = Fields[aID.index(self.AreaID)]
            Areas.remove(self.Area0)
            self.Areas_Meas = list(filter(lambda a: a.ID in aID, Areas))
            for item in self.Areas_Meas:
                item.Fields = Fields[aID.index(item.ID)]
            
        self.Areas_Xtras = list(filter(lambda a: a not in self.Areas_Meas,Areas))
        
        #   Falta separar Areas_Meas de Areas_Checks
        if self.Valid==1:
            Change=True
            while Change:
                Change=False
                parents = [self.Area0.ID]
                for a in self.Areas_Meas:
                    if a.ParentAreaID in parents:
                        Change=True
                        self.Areas_Checks.append(a)
                        parents.append(a.ID)
                self.Areas_Meas = list(filter(lambda a: a not in self.Areas_Checks, self.Areas_Meas))       
                
        ### AreaVariables  (sólo para Area0)
        if self.Valid==1:
            Q = 'Name, Definition'
            T = 'AreaVariables'
            W =f'FileID={self.FileID} AND AreaID={self.Area0.ID} '
            W+= "AND Name IN ('FieldWidth', 'FieldHeight', 'FieldWidthPx', 'FieldHeightPx')"
            Name,Def = list(zip(*self.DB.query(Q,T,W)))
            self.Area0.FieldWidth    = float(Def[Name.index('FieldWidth'   )])
            self.Area0.FieldHeight   = float(Def[Name.index('FieldHeight'  )])
            self.Area0.FieldWidthPx  =   int(Def[Name.index('FieldWidthPx' )])
            self.Area0.FieldHeightPx =   int(Def[Name.index('FieldHeightPx')])
            self.Area0.PixelSize     = self.Area0.FieldWidth / self.Area0.FieldWidthPx
            
            ## Fields Collected
            Q = 'count(*)'
            F = 'Fields'
            W = f'FileID={self.FileID} AND AreaID={self.AreaID} AND DataCollected=1'
            self.Area0.FieldsCollected = self.DB.query(Q,F,W)

        ### Calculo superficies
        self.supCalculator()
        
    def supCalculator(self):
        '''Calcula las áreas y define el límite de la medición
           Tarda tan poco, que lo cargo siempre
        '''
        ### Variables a calcular
        self.FieldPositions = None
        sup = {'All'      : None,
               'Fields'   : None,
               'Collected': None,
               'Polygon'  : None,
              }
        
        if self.Valid==0:
            return
        
        ### Superficies
        sup['All'] = pi * (self.Area0.R)**2
        oneField = self.Area0.FieldWidth * self.Area0.FieldHeight
        sup['Fields'] = self.Area0.Fields * oneField
        sup['Collected'] = self.Area0.FieldsCollected * oneField
        
        ### Limit Polygon
        if self.Area0.FieldsCollected == 0:
            self.Area0.sup = sup
            return
        
        #   Fields
        Q = 'FieldID,X,Y FROM Fields'
        W =f'FileID={self.FileID} AND AreaID={self.AreaID} AND DataCollected=1'
        Fpos = self.DB.query(Q,W)
        if isinstance(Fpos, tuple):
            Fpos = [Fpos]
        Fpos = sorted(Fpos, key=lambda x: x[0])
        self.FieldPositions = Fpos
        _, X,Y = zip(*Fpos)
            
        #   Cuantifico alturas y busco extremos         
        W = self.Area0.FieldWidth
        H = self.Area0.FieldHeight

        y = list(map(lambda x: int(round(x)),(array(Y)-Y[0])/H))
        l = sorted(list(set(y)))                            # levels
        il = [y.index(li)                for li in l      ] # index left
        ir = [len(y)-1-y[::-1].index(li) for li in l[::-1]] # index right
        
        #   Polígono
        Poly = []
        for i in il:
            Poly.append([X[i]-W/2,Y[i]-H/2])
            Poly.append([X[i]-W/2,Y[i]+H/2])
        for i in ir:
            Poly.append([X[i]+W/2,Y[i]+H/2])
            Poly.append([X[i]+W/2,Y[i]-H/2])
        Poly.append(Poly[0])    
        sup['Polygon'] = Poly    
            
        self.Area0.sup = sup
        
    def loadFeatures(self):
        '''Carga las Features
        '''
        # Datos a cargar
        self.FeaturesN = None
        self.Features  = None
        
        ### Cargo todo
        Features = []
        
        if self.Valid==0:  return
        
        ### Lista de Fields que interesan
        FieldPos = self.FieldPositions 
        #   Construyo FS: un string con la lista de Fields
        Fields   = list(map(lambda x: "'" + str(x[0]) + "'", FieldPos)) 
        FS = "("
        for item in Fields:
            FS+= item +", "
        FS = FS[:-2]+")"
        
        ### Lista de Features
        Q = 'FeatureID,FieldID,BB_Top,BB_Left,BB_Height,BB_Width'
        F = 'Features'
        W =f'FileID={self.FileID} '
        W+=f'AND FieldID IN {FS}'
        Feats = self.DB.query(Q,F,W)
        self.FeaturesN = len(Feats)
        
        if Feats!=[]:
            if isinstance(Feats, tuple):
                Feats = [Feats]
                
            Feats = sorted(Feats, key=lambda x: x[0])
        else:
            return            
            
        #   Construyo FeaS: un string con la lista de Features
        Featus   = list(map(lambda x: "'" + str(x[0]) + "'", Feats)) 
        FeaS = "("
        for item in Featus:
            FeaS+= item +", "
        FeaS = FeaS[:-2]+")"

        #   Tabla de Ranks
        Q = 'Rank,Name'
        RankTable = self.DB.query( Q + ' FROM Ranks')
        RName = dict(RankTable)   # RName[RankID]
        
        #   Tabla de Clases
        Q = 'ClassID,Name,Rank,UseForClassification'
        Class_Defs = self.DB.query( Q + ' FROM Classes')
        CDefs = {}
        for ClassID,Name,Rank,UseForClassification in Class_Defs:
            CDefs[ClassID] = { 'ClassID':ClassID,
                               'Name':Name,
                               'Rank':Rank,
                               'RankName':RName[Rank],
                               'UseForClassification':int(UseForClassification)
                             }
        
        #   Asignación de ClassID
        Q = 'FeatureID, ClassID'
        F = 'feature_classifications'
        W =f'FileID={self.FileID} AND FeatureID IN {FeaS}'
        ClassTable = self.DB.query(Q,F,W)
        ClassID = dict(ClassTable)         # ClassID[FeatureID]
        
        for row in Feats:
            F = Feature(row)
            # filto por el FieldID
            FP = list(filter(lambda x: x[0]==row[1] ,FieldPos))[0]
            F.FieldX = FP[1]
            F.FieldY = FP[2]
            
            ##### Reparación de ClassID
            CID = ClassID[F.ID]
            #if CID > 31: CID -= 31  ### Línea de corrección 1/2
            #if CID < 7 : CID += 31  ### Línea de corrección 2/2
            F.Class  = CDefs[CID]
            
            Features.append(F)
        self.Features = Features

    def loadResults(self):
        '''Carga Results como un diccionario de cada Feature
        '''
        if self.Features is None:
            return
        
        ### Index of FeatureID and void dict Measurements
        iFeature={}     # iFeature[FeatureID] = index of Features
        FeatureS = '('  # String con lista de Features
        for i,item in enumerate(self.Features):
            item.Measurements = {}
            iFeature[item.ID]=i
            FeatureS += f"'{item.ID}', "
        FeatureS = FeatureS[:-2] + ')'
            
        ### Tabla uMeasurements:  Meas[MeasurementID] = {col: val}
        Meas = {}
        Q = 'uMeasurementID, Type, Definition, Units, Name'
        R = self.DB.query(Q+' FROM uMeasurements')
        for uMeasurementID, Type, Definition, Units, Name in R:
            Meas[uMeasurementID]={'Type': Type, 
                                  'Definition': Definition, 
                                  'Units': Units, 
                                  'Name': Name,
                                 }
            
        ### Cargo todos los uResults
        print('Loading Results...', end='')
        Q = 'FeatureID, uMeasurementID, Value, Error FROM uResults'
        W =f"FileID={self.FileID} AND FeatureID IN {FeatureS}"
        R = self.DB.query(Q,W)
        print('  done.-')
        for FeatureID, uMeasurementID, Value, Error in R:
            M = Meas[uMeasurementID]
            self.Features[iFeature[FeatureID]].Measurements[M['Definition']] = {
                'ID'   :uMeasurementID,
                'Type' :M['Type'],
                'Name' :M['Name'],
                'Value':Value,
                'Error':Error,
                'Units':M['Units'],
            }
    ####################################################################
    ############################## Reports
    def reportFile(self):
        '''String con información del archivo
        '''
        oP = '/'.join(self.Path.split('/')[1:-1])
        oF = self.Path.split('/')[-1]
        ou = f'fID: {self.FileID}   {oF}\n{self.Date}\n({self.Extra}): {oP}'
        if self.Valid==0:
            ou+='\nNot Valid'
        return ou
           
    def _pLimits(self,xmin,xmax,ymin,ymax):
        '''ayuda a poner los límites del dibujo
           (los shapes no definen límites)
        '''
        plot([xmin,xmax],[ymin,ymax],'w.')
        
    def _printReportFile(self,x,y,**kwargs):
        '''Texto ReportFile en el dibujo
        '''
        text(x,y,self.reportFile(),
             verticalalignment='top',
             horizontalalignment='left',
             **kwargs)
        
    def _PlotArea(self, Area,marker='x'):
        '''Dibuja un área
        '''
        try:
            Area0 = True
            col='g'
            poly  = Area.sup['Polygon']
            fill  = True
        except:
            Area0 = False
            col='#333333'
            poly  = None
            fill = False
            if marker=='.': markerS = 5
            else          : markerS = 7

        if Area.Shape == 3:
            X,Y,R = Area.X,Area.Y,Area.R
            circle1 = Circle([X,Y], R, color=col,Fill=fill, alpha=.4)
            gca().add_artist(circle1)
            text(X,Y+R,Area.Name, va='bottom', ha='center', color=col)
        else:
            X,Y = Area.X,Area.Y
            shiftX = 0.7
            shiftY = 0
            if Area.Name=='Contraste': shiftY= 0.9
            if Area.Name=='Cobalto'  : shiftY=-0.9

            keys={'marker'    :marker, 
                  'markersize':markerS,
                  'alpha'     :.7}

            if marker!='.':
                keys['color']=col
                text(X+shiftX,Y+shiftY,Area.Name,va='center',ha='left',alpha=0.95)
            else:
                keys['label']=Area.Name[Area.Name.find('(')+1:-1]

            plot(X,Y,'o', **keys)

        if Area0:
            plot(*zip(*poly),'b-',lw=1)
            
    def plotAreas(self):
        '''Dibuja todas las áreas
        '''
        self._pLimits(25,100,25,100)
        
        for Area in self.Areas_Xtras:
            self._PlotArea(Area)
        for Area in self.Areas_Meas:
            self._PlotArea(Area,marker='o')
        for Area in self.Areas_Checks:
            self._PlotArea(Area,marker='.')

        if self.Valid==1:
            self._PlotArea(self.Area0)
    
        self._printReportFile(23,102)
        
        title ('Areas')
        xlabel(r'$X$ Stage position $[mm]$',fontsize=15)
        ylabel(r'$Y$ Stage position  $[mm]$',fontsize=15)
        legend(loc=1)
        axis('equal')
        
#------------------------------------------------------------------------
class Area:
    def __init__(self,row):
        self.AreaID = row[0]
        self.ID = self.AreaID
        self.Name   = row[2]
        self.DataCollected = int(row[4])
        self.Fields = 0
        
        ### Definition is XML
        Definition = Xml(row[3])
        self.Shape = int(Definition.e.attrib['Shape'])
        self.Type  = int(Definition.e.attrib['Type'])
        self.ParentAreaID = int(Definition.e.attrib['ParentAreaID'])
        self.OriginalFeatureID = int(Definition.e.attrib['OriginalFeatureID'])
        
        ### Positions
        R = []
        for p in Definition.e[0]:
            R.append([float(p.attrib['X']),float(p.attrib['Y']),float(p.attrib['Z'])])
        if self.Shape == 0:
            ## Punto
            self.X,self.Y,self.Z = R[0]
        else:
            ## Círculo
            self.X,self.Y,self.Z = R[3]
            X0,Y0,Z0 = R[0]
            self.R = dist([self.X,self.Y],[X0,Y0])

#------------------------------------------------------------------------            
class Feature:
    def __init__(self,row):
        # FeatureID,FieldID,BB_Top,BB_Left,BB_Height,BB_Width
        self.FeatureID = row[0]
        self.ID        = self.FeatureID
        self.BB_Top    = row[2]
        self.BB_Left   = row[3]
        self.BB_Height = row[4]
        self.BB_Width  = row[5]
        self.Class     = None
        self.FieldX    = None
        self.FieldY    = None
        
#------------------------------------------------------------------------
# Chau
