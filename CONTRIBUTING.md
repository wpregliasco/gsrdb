## Contribuciones

Este proyecto se desarrolló por iniciativa del _Grupo de Física Forense_ del _Centro Atómico Bariloche_ 
con las horas de trabajo puesta por:

* __Willy Pregliasco__ @wpregliasco: dirección y coordinación del proyecto
* __Martín Onetto__ @onezan

Otras contribuciones y consultas son bienvenidas y basta con abrir un Issue o un Merge Request para incluir el aporte.
