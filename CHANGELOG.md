## 0.5.0 (2021-04-25)

### Bugs fixed

- [_transferencia_ [#1](https://gitlab.com/wpregliasco/gsrdb/-/issues/1)]: sustitución de columna `Rank` de las tablas (RANK es una palabra reservada de MySQL y trae problemas en los _queries_). 

- [_transferencia_ [#3](https://gitlab.com/wpregliasco/gsrdb/-/issues/3)]: el programa `loadMdbs` chequea el último `FileID` utilizado y carga a partir de ese número. Esto permite realizar cargas que amplíen una base de datos. __Cuidado:__ no hay chequeo de archivos duplicados. 

- [_transferencia_ [#6](https://gitlab.com/wpregliasco/gsrdb/-/issues/6)] ya se pueden hacer múltiples cargas y no se agrega dos veces el mismo archivo a MySQL.

### New features

- [_instalación_]: se puede replicar el environment de anaconda para que todo anden sin problemas las dependencias (ver la [documentación de instalación](https://wpregliasco.gitlab.io/gsrdb/instalacion.html)).

- [_instalación_]: se puede instalar el módulo de python usando `pip install -e .` (ver la [documentación de instalación](https://wpregliasco.gitlab.io/gsrdb/instalacion.html)).

- [_transferencia_ [#2](https://gitlab.com/wpregliasco/gsrdb/-/issues/2)]: el programa `loadMdbs.py` no carga archivos que no tienen un scan de la muestra.

- [_documentación_]: se incluyó el análisis de la estructura de la base de datos y una descripción completa de las tablas. 

- [_transferencia_ [#5](https://gitlab.com/wpregliasco/gsrdb/-/issues/5),[#4](https://gitlab.com/wpregliasco/gsrdb/-/issues/4)]: agregamos el programa `post.py` que hace un reacondicionamiento de las tablas MySQL.

- [_transferencia_ [#8](https://gitlab.com/wpregliasco/gsrdb/-/issues/8)]: comandos para salvar y cargar backups MySQL.

- [_transferencia_ ]: comando para carga rápida de la base de datos (`qload.py`) y documentación para _quickstart_
