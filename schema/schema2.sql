CREATE TABLE `Files`
 (
	`FileID`		int,
	`Path`		varchar (64) NOT NULL,
	`Extra`		varchar (32),
	`Date`		DATE,
	`AreaID`		VARCHAR(64)
);

CREATE TABLE `GlobalVariables`
 (
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (1024)
);

CREATE TABLE `Areas`
 (
	`FileID`		int NOT NULL,
	`AreaID`		int,
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (1024),
	`DataCollected`		char NOT NULL
);

CREATE TABLE `AreaVariables`
 (
	`FileID`		int NOT NULL,
	`AreaID`		int NOT NULL,
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (255) NOT NULL
);

CREATE TABLE `Fields`
 (
	`FileID`		int NOT NULL,
	`FieldID`		int,
	`AreaID`		int NOT NULL,
	`X`		float NOT NULL,
	`Y`		float NOT NULL,
	`Z`		float NOT NULL,
	`T`		float NOT NULL,
	`R`		float NOT NULL,
	`DataCollected`		char NOT NULL
);

CREATE TABLE `Thresholds`
 (
	`FileID`		int NOT NULL,
	`ThresholdID`		int,
	`Name`		varchar (64) NOT NULL,
	`Min`		int NOT NULL,
	`Max`		int NOT NULL
);

CREATE TABLE `Features`
 (
	`FileID`		int NOT NULL,
	`FeatureID`		int,
	`FieldID`		int NOT NULL,
	`ThresholdID`		int NOT NULL,
	`BB_Top`		int NOT NULL,
	`BB_Left`		int NOT NULL,
	`BB_Height`		int NOT NULL,
	`BB_Width`		int NOT NULL,
	`Rank_`		int NOT NULL
);

CREATE TABLE `Measurements`
 (
	`FileID`		int NOT NULL,
	`MeasurementID`		int,
	`Type`		int NOT NULL,
	`Definition`		text (255) NOT NULL,
	`Units`		varchar (20),
	`Name`		varchar (64) NOT NULL
);

CREATE TABLE `Results`
 (
	`FileID`		int NOT NULL,
	`FeatureID`		int NOT NULL,
	`MeasurementID`		int NOT NULL,
	`Value`		float NOT NULL,
	`Error`		float NOT NULL
);

CREATE TABLE `Classes`
 (
	`ClassID`		int,
	`Name`		varchar (100) NOT NULL,
	`Rank_`		int NOT NULL,
	`UseInMorphFilter`		char NOT NULL,
	`UseInEDFilter`		char NOT NULL,
	`UseForClassification`		char NOT NULL,
	`StoreResults`		char NOT NULL
);

CREATE TABLE `feature_classifications`
 (
	`FileID`		int NOT NULL,
	`FeatureID`		int NOT NULL,
	`ClassID`		int NOT NULL
);

CREATE TABLE `Ranks`
 (
	`Rank_`		int,
	`Name`		varchar (100) NOT NULL
);

CREATE TABLE `uMeasurements`
 (
	`uMeasurementID`		INT AUTO_INCREMENT PRIMARY KEY,
	`Type`		INT,
	`Definition`		VARCHAR (255) NOT NULL UNIQUE,
	`Units`		VARCHAR (20),
	`Name`		VARCHAR (64)
);

CREATE TABLE `uResults`
 (
	`FileID`		INT NOT NULL,
	`FeatureID`		INT NOT NULL,
	`uMeasurementID`		INT NOT NULL,
	`Value`		FLOAT NOT NULL,
	`Error`		FLOAT NOT NULL
);

