
           SET autocommit=0;
           SET unique_checks=0;
           SET foreign_key_checks=0;
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("DBVersion","2.0");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("LayoutName","19 Stub Circular");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ChamberXML","<ChamberSettings Azimuth=""270"" Takeoff=""35"" SCDist=""50"" ViewWD=""8.5"" ProbeAngle=""35"" WindowType=""5""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("StageXML","<StageSettings Type=""XYZTR""><Axis Name=""X"" Max=""0"" Min=""0"" Dirn=""1""/><Axis Name=""Y"" Max=""0"" Min=""0"" Dirn=""1""/><Axis Name=""Z"" Max=""0"" Min=""0"" Dirn=""1""/><Axis Name=""Tilt"" Max=""0"" Min=""0""><Posn X=""0"" Y=""62.5"" Z=""0""/><Dirn X=""1"" Y=""0"" Z=""0""/></Axis><Axis Name=""Rotate"" Max=""360"" Min=""-360""><Posn X=""0"" Y=""0"" Z=""0""/><Dirn X=""0"" Y=""0"" Z=""1""/></Axis></StageSettings>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("MatrixSetup","<ReAcqSetup AcquisitionTime=""20"" QuantSetup="""" AreaName=""""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("RecipeSetup","<RecipeSetup Description="""" Recipe=""GSR PDI"" ClassScheme=""GSR"" QuantScheme=""gsr"" Locked=""0"" UseSmartmap=""0""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ClassSetupModifiedDate","13.8.2012 20:15:03");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("QuantAppConc","0");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ClassificationMethod","1");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ClassifyRejected","0");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("QuantSetup","<QuantSetup VersionNumber=""2"" ElementListType=""0"" ProcessingOption=""1"" CombinedElement=""0"" AutoSelectQuantLines=""-1"" SampleType="""" SpectrumLabel=""1"" AlphaPeaksOnly=""0""><DeconvolutionElement AtomicNumber=""6""/></QuantSetup>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionDate","7/8/2017 12:35:31 AM");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionDateAsCurrency","42924.0247");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("PeakCountValues","|519109|42924.025|519835|42924.0675");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ThresholdStandardValues","|124.7569|42924.025|125.0325|42924.0294|125.2522|42924.0339|125.6898|42924.043|125.3333|42924.0473|125.4416|42924.0551|125.5733|42924.0595|125.3223|42924.0633|125.9353|42924.0675|126.3333|42924.0736");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("CalibWidthFOV","0.30224321133412");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionMagnification","295");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionImageWidth","2048");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionImageHeight","1536");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("DBState","2");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionStopReason","Normal");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("AcquisitionDurationMins","71.6333333251532");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ReAcqSetup","<ReAcqSetup ScanMethod=""0"" DualAcquisition=""0"" DeleteOriginal=""0"" AcquisitionTime=""10""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("IPAQuantSetup","<IPAQuantSetup OptimizationElement=""27"" Livetime=""10"" OptimizationInterval=""3600"" HaltOnFail=""0"" AreaName=""Cobalto"" ReportCountRate=""0"" ReportLiveTime=""0"" ReportDeadTime=""0"" ReportProcessTime=""5"" ReportColumnHV=""0"" ReportSavedTime="""" ReportStrobeCounts=""0"" ReportStrobeEnergy=""0"" ReportStrobeFWHM=""0"" ReportPeakCounts=""0"" ReportPeakEnergy=""0"" ReportPeakFWHM=""0""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("QuantOptSpectrum",NULL);
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("IPAXraySetup","<IPAXraySetup FullRange=""20"" ProcessTime=""3"" Time1=""4"" Time2=""5"" NumberOfChannels=""2048"" Passes=""1"" SaveSpectra=""1"" SaveChordlist=""1"" CentreLongestChord=""0"" SaveImages=""1"" HVOffAtEnd=""0"" FilamentOffAtEnd=""0""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("TermSetup","<TermSetup FieldLimit=""0"" FieldRankLimit=""0"" FieldTimeLimit=""0"" FieldRank=""1"" AreaLimit=""0"" AreaRankLimit=""0"" AreaTimeLimit=""0"" AreaRank=""1"" SampleLimit=""0"" SampleRankLimit=""0"" SampleTimeLimit=""0"" SampleRank=""1""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("PDSetup","<PDSetup PreProcData="""" BinProcData="""" FirstPassDwell=""3"" SecondPassDwell=""10"" LeadingEdge=""20"" TrailingEdge=""20"" ImageWidth=""2048"" Source=""0"" MinSize=""1"" Magnification=""295"" AutoFocus=""0"" GuardZone=""0"" GuardZoneActive=""0""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("THSetup","<THSetup NumThresholds=""1"" StdPosX1=""229"" StdPosY1=""232"" StdPosX2=""469"" StdPosY2=""25"" BeamCompIntervalSecs=""300"" AreaName=""Contraste"" BeamCompHighDev=""30"" BeamCompLowDev=""20"" BeamCompSampleWidth=""50"" ThresholdSaveTime=""7/7/2017 9:41:24 PM"" CalibSaveTime=""7/7/2017 9:40:37 PM"" ActiveThreshold=""0"" DetectedFeatures=""17"" ThresholdLow0=""74"" ThresholdHigh0=""255"" ThresholdLow1=""128"" ThresholdHigh1=""255"" ThresholdLow2=""128"" ThresholdHigh2=""255"" ThresholdLow3=""128"" ThresholdHigh3=""255"" ThresholdLow4=""128"" ThresholdHigh4=""255"" ThresholdLow5=""128"" ThresholdHigh5=""255"" ThresholdLow6=""99"" ThresholdHigh6=""236""/>");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("CalibMapStorage","쿐놡");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ThresholdMapStorage","쿐놡");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("CalibLinesetStorage","쿐놡");
INSERT INTO `GlobalVariables` (`Name`, `Definition`) VALUES ("ThresholdLinesetStorage","쿐놡");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (1,"characteristic");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (2,"consistent Sb Ba Pb");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (3,"Lead-free / non-toxic");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (4,"consistent Lead-free / non-toxic");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (5,"enviromental");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (6,"6");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (100,"Unclassified");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (110,"Rejected (Manual)");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (120,"Rejected (ED)");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (130,"Rejected (Morph)");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (200,"Multi-classified");
INSERT INTO `Ranks` (`Rank_`, `Name`) VALUES (900,"Marked");
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (7,"Sb Ba Pb",1,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (8,"Sb Ba Pb Sn",1,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (9,"Ba Si Ca",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (10,"Ba Sb",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (11,"Pb Sb",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (12,"Ba Al",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (13,"Pb Ba",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (14,"Pb",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (15,"Sb",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (16,"Ba",2,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (17,"Ti Zn Gd",3,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (18,"Cu Sn Ga",3,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (19,"Ti Zn Cu",4,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (20,"Ti Zn Sn",4,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (21,"Sr",4,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (22,"Ti  Zn",4,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (23,"Cu Zn",4,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (24,"Ni",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (25,"Sn",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (26,"Au",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (27,"lighter flint",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (28,"Fe",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (29,"Cu",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (30,"Zn",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (31,"S",5,0,0,1,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (32,"Unclassifed",100,0,0,0,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (33,"Rejected (Manual)",110,0,0,0,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (34,"Rejected (ED)",120,0,0,0,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (35,"Rejected (Morph)",130,0,0,0,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (36,"Multi-classified",200,0,0,0,1);
INSERT INTO `Classes` (`ClassID`, `Name`, `Rank_`, `UseInMorphFilter`, `UseInEDFilter`, `UseForClassification`, `StoreResults`) VALUES (37,"Marked",900,0,0,0,1);

           COMMIT;
           SET unique_checks=1;
           SET foreign_key_checks=1;