CREATE TABLE `GlobalVariables`
 (
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (1024)
);

CREATE TABLE `Areas`
 (
	`AreaID`		int,
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (1024),
	`DataCollected`		char NOT NULL
);

CREATE TABLE `AreaVariables`
 (
	`AreaID`		int NOT NULL,
	`Name`		varchar (64) NOT NULL,
	`Definition`		text (255) NOT NULL
);

CREATE TABLE `Fields`
 (
	`FieldID`		int,
	`AreaID`		int NOT NULL,
	`X`		float NOT NULL,
	`Y`		float NOT NULL,
	`Z`		float NOT NULL,
	`T`		float NOT NULL,
	`R`		float NOT NULL,
	`DataCollected`		char NOT NULL
);

CREATE TABLE `Thresholds`
 (
	`ThresholdID`		int,
	`Name`		varchar (64) NOT NULL,
	`Min`		int NOT NULL,
	`Max`		int NOT NULL
);

CREATE TABLE `QuantSetup`
 (
	`AtomicNumber`		int NOT NULL,
	`Selected`		char NOT NULL,
	`QuantLine`		int NOT NULL
);

CREATE TABLE `Features`
 (
	`FeatureID`		int,
	`FieldID`		int NOT NULL,
	`ThresholdID`		int NOT NULL,
	`BB_Top`		int NOT NULL,
	`BB_Left`		int NOT NULL,
	`BB_Height`		int NOT NULL,
	`BB_Width`		int NOT NULL,
	`Rank`		int NOT NULL
);

CREATE TABLE `Measurements`
 (
	`MeasurementID`		int,
	`Type`		int NOT NULL,
	`Definition`		text (255) NOT NULL,
	`Units`		varchar (20),
	`Name`		varchar (64) NOT NULL
);

CREATE TABLE `Results`
 (
	`FeatureID`		int NOT NULL,
	`MeasurementID`		int NOT NULL,
	`Value`		float NOT NULL,
	`Error`		float NOT NULL
);

CREATE TABLE `Classes`
 (
	`ClassID`		int,
	`Name`		varchar (100) NOT NULL,
	`Rank`		int NOT NULL,
	`UseInMorphFilter`		char NOT NULL,
	`UseInEDFilter`		char NOT NULL,
	`UseForClassification`		char NOT NULL,
	`StoreResults`		char NOT NULL
);

CREATE TABLE `Criteria`
 (
	`CriterionID`		int,
	`CriteriaType`		int NOT NULL,
	`Name`		varchar (100) NOT NULL,
	`Definition`		text (255) NOT NULL
);

CREATE TABLE `Criteria_Measurements`
 (
	`CriterionID`		int NOT NULL,
	`MeasurementID`		int NOT NULL
);

CREATE TABLE `feature_classifications`
 (
	`FeatureID`		int NOT NULL,
	`ClassID`		int NOT NULL
);

CREATE TABLE `feature_criteria`
 (
	`FeatureID`		int NOT NULL,
	`CriterionID`		int NOT NULL
);

CREATE TABLE `class_criteria`
 (
	`ClassID`		int NOT NULL,
	`CriterionID`		int NOT NULL,
	`Excluded`		char NOT NULL
);

CREATE TABLE `Images`
 (
	`FieldID`		int NOT NULL,
	`ImageData`		varchar (255) NOT NULL
);

CREATE TABLE `ChordLists`
 (
	`FeatureID`		int NOT NULL,
	`ChordList`		varchar (255) NOT NULL
);

CREATE TABLE `Spectra`
 (
	`FeatureID`		int NOT NULL,
	`XrayData`		varchar (255) NOT NULL
);

CREATE TABLE `Ranks`
 (
	`Rank`		int,
	`Name`		varchar (100) NOT NULL
);

