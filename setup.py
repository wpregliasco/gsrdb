import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='dbdiver-gsr-gff',
    version='1.0.0',
    author="Willy Pregliasco",
    author_email="willy.pregliasco@gmail.com",
    description="class to access gsr databases",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://wpregliasco.gitlab.io/gsrdb/",

    packages=setuptools.find_packages(include=['dbdiver', 'dbdiver.*']),

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.6',
)
