:orphan:

******************************************************
Transferencia de archivos ``.mdb`` a la base de datos
******************************************************

La idea es agregar la información que se encuentra en el microscopio, 
guardada en diferentes archivos ``.mdb`` a una base de datos común en 
formato ``MySQL``.

La transferencia consiste en las siguientes operaciones:

#. creamos la base de datos de destino y transferimos la estuctura de las tablas 
#. transferimos los datos comunes (1 tabla)
#. transferimos los datos de cada una de las muestras
#. hacemos un acondicionamiento de la base de datos.
#. hacemos un backup de la nueva base de datos.

**Los dos primeros pasos** los realizamos sólo cuando sospechamos un cambio 
de formato, que puede ser por un cambio de versión del *INCA* o por un 
equipo diferente. Eso no podemos saberlo porque hasta ahora, sólo procesamos 
un conjunto de muestras. Son las **operaciones de desarrollo**. 

**Los pasos 3, 4 y 5** son necesarios con cada nuevo conjunto de muestras y no 
necesariamente 
hay que volver a chequear la estructura de las tablas. Son las **operaciones de carga**.

Operaciones de desarrollo
=========================

Estas operaciones requieren intervención y chequeos manuales.
Esta etapa merece un poco más de trabajo. 

El trabajo se hace todo desde el directorio de scripts.

Está dividido en pasos para poder intervenir en los pasos intermedios y 
corregir problemas inesperados. 

Preparación
  * Poner los datos a procesar en una carpeta
  * elegir un archivo ``.mdb`` para muestra del *schema*
  * configurar los paths de ``setup.ini`` (tienen que existir)

.. _script-0schema:

``schema0.py``
  * carga el *schema* de la base de datos de ejemplo
  * produce una salida original ``schema0.sql`` y otra con pequeñas correcciones *ad hoc* ``schema1.sql``
  * hace una lista de tablas ``tables.dat``

Clasificación de las tablas
  Esto hay que hacerlo a mano. 
  Hay que separar el contenido de ``tables.dat`` en dos tablas:
  * ``tables_sample.dat`` tablas cuyo contenido cambia en cada muestra 
  * ``tables_common.dat`` tablas generales, que tienen información del microscopio
  Las tablas que no estén incluidas en ningun de las dos listas, serán ignoradas 
  en el procesamiento

.. _script-1schema:

``schema1.py``
  * Genera ``schema2.sql``
  
    * filtra sólo las tablas que están presentes en alguno de los dos listados
    * agrega la tabla ``Files`` con la información de los archivos
    * agrega el campo ``FileID`` a las tablas de ``tablas_sample``
    * agrega las tablas únicas ``uMeasurements`` y ``uResults``
    * carga la lista de elementos a ``uMeasurements``
    
  * Genera ``tables_common.sql`` 
  
    * Tiene la información para cargar las tablas comunes a la base de datos

  * Genera la base de datos (si existe la borra) e importa el *schema* y las tablas comunes.

.. image:: _static/schema0.svg
   :alt: Desarrollo_1

.. warning::
  Este paso me costó mucho porque es recursivo: no puedo saber si el contenido 
  de la tabla cambia con cada muestra en este momento, porque todavía no leí las 
  tablas (sólo miramos el *schema*).
  Hay que llegar al final del proceso, analizar el contenido de las tablas y volver 
  a hacer la clasificación. 

  Esto ya está hecho y es lo que está configurado en el repositorio. 

  En algún futuro sería interesante poder leer las imágenes.

.. _script-loadMdbs:

Operaciones de carga: armado de base MySQL
==========================================

Toda la operación está condensada en un único programa ``loadMdbs.py`` que tiene 
dos modos de funcionamiento:

* si no existe el archivo ``dataDir/dirList.dat``: 
    escanea los datos y construye ese archivo. 

    La idea es tener la oportunidad de editar la lista y decidir qué 
    archivos procesar.

* si ya existe  la lista de datos:
    carga las tablas de cada muestra en la base de datos *MySQL*.  

.. image:: _static/loadMdbs.svg
   :alt: Desarrollo_2 

.. note::
  Se pueden hacer cargas sucesivas y sólo se agregarán los archivos que no están en la base de datos.

.. _script-post:

Operaciones de carga: acondicionamiento
=======================================

Hicimos un script ``post.py`` que hace el acondicionamiento de la 
base de datos. 
Las operaciones que realiza son:

* Agrega una columna ``AreaID`` a la tabla ``Files`` conteniendo 
  el ID de la muestra que está analizada en cada archivo.
  Buscar ese área es muy indirecto y esta asignación facilita mucho el análisis posterior. 

* La tabla ``Measurements`` es una tabla índice con el tipo de mediciones que se le
  pueden hacer a cada feature. Pero los IDs cambian en cada archivo. Se ahorra mucho espacio 
  descartando las filas de contenido duplicado, y esa es la tabla nueva ``uMeasurements`` que 
  tiene un merge de todas las mediciones que hay en los archivos. 

* Pero al hacer el paso anterior, necesitamos referir los resultados de las mediciones
  a esa medición única. La asignación correcta la agregamos a la tabla ``uResults``
  cuya única diferencia con ``Results`` es que el campo de ID de las mediciones
  señala a la tabla ``uMeasurements``

* con estas operaciones realizadas, borramos el contenido de las tablas ``Measurements`` y ``Results``
  
  
Backup MySQL
============

La base de datos cargada se puede exportar a otra máquina para hacer el análisis de las partículas. 
El proceso de transferencia está terminado. A lo sumo se puede agrandar la base de datos con más archivos. 

La manera de exportar el archivo es con el comando (implementado en ``backup_database.py``):

.. code-block:: bash

   mysqldump -ugsruser gsr0 \
             --single-transaction \
             --quick \
             --lock-tables=false > gsr0_Backup_00.sql

Este es un archivo de texto que puede achicarse considerablemente haciendo un zip. 

La base de datos se importa con el comando (implementado en ``load_database.py``):

.. code-block:: bash
   
   mysql -ugsruser gsr0 < gsr0_Backup_00.sql

.. _script-backupDB:

``backup_database.py``
   * utiliza el formato de nombres: ``Backup_nn_yyyy-mm-dd.sql`` 
      * ``nn`` es un índice incremental de los backups
      * ``yyyy-mm-dd`` es la fecha en que se hizo el backup
   * escribe y lee en el directorio de los datos ``dataDir``
   * genera un backup con índice incremental respecto de los ``.zip`` y ``.sql`` para nunca pisar un backup previo
   * hace un ``.zip`` del resultado para guardarlo y transferirlo mejor.

.. _script-loadDB:

``load_database.py``
   * utiliza el mismo formato de nombres: ``Backup_nn_yyyy-mm-dd.sql`` 
   * escribe y lee en el directorio de los datos ``dataDir``
   * carga el último backup que hay **en formato** ``.sql``
   * al hacer la carga **borra el contenido previo de la base de datos**.