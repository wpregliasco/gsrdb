:orphan:

***********
Quickstart
***********

.. toctree::
    :maxdepth: 2

Instalación
===========

La instalación tiene sus detalles que se pueden consultar en el documento: :doc:`instalacion`.
Se pueden resumir los pasos en:

* instalar el Python via Anaconda 
* instalar la base de datos Mysql con un usuario sin password
* instalar el git

Nos ubicamos en un directorio de proyectos adonde queremos instalar este programa. 

.. code-block:: bash

    sudo apt install mdbtools
    git clone https://gitlab.com/wpregliasco/gsrdb.git
    cd gsrdb
    conda deactivate
    conda update -n base -c defaults conda
    conda env remove --name gsr
    conda env create -f environment.yml
    conda activate gsr
    pip install -e .

Carga DB
========

Nos paramos en el directorio ``scripts`` del proyecto 
y activamos el entorno de Python con el comando: ``conda activate gsr``

Hay que editar el archivo ``setup.ini`` con los directorios del caso. 

Si no tenemos hecha la base de datos MySQL
------------------------------------------

Corriendo el programa

.. code-block:: bash

    (gsr) $ python qload.py

se cargarán todos los archivos que se encuentran en los subdirectorios de Datos
a la base MySQL. Este proceso puede ser lento y se recomienda lanzarlo en una buena 
máquina. El resultado será un archivo de backup que puede ser importado en otro ambiente.

Si contamos con un archivo de backup 
------------------------------------

Colocamos ese archivo en el directorio de datos. 
Si está comprimido, lo descomprimimos y corremos el comando

.. code-block:: bash

    (gsr) $ python load_database.py


Análisis de Datos
=================

Bli