:orphan:

***************************
Listado de scripts
***************************

.. toctree::
    :maxdepth: 2

+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| script                                        | etapa                | descripción                                                        | 
+===============================================+======================+====================================================================+
| :ref:`schema0.py<script-0schema>`             | (*desarrollo*)       | Interpreta el *schema* de las bases de datos ``.mdb``              |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`schema1.py<script-1schema>`             | (*desarrollo/carga*) | Prepara el *schema* de MySQL y carga datos comunes                 |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`loadMdbs.py<script-loadMdbs>`           | (*carga*)            | Genera ``dirlist.dat``/Carga el contenido de los ``.mdb`` en MySQL |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`post.py<script-post>`                   | (*carga*)            | Acondicionamiento final de MySQL, después de la carga              |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`backup_database.py<script-backupDB>`    | (*carga*)            | Backup del resultado final MySQL                                   |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`load_database.py<script-loadDB>`        | (*carga*)            | Carga una base de datos MySQL                                      |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+
| :ref:`qload.py<script-loadDB>`                | (*carga*)            | Quick Load: hace todo sin preguntar para cargar a MySQL            |
+-----------------------------------------------+----------------------+--------------------------------------------------------------------+


Todos los scripts utilizan el archivo de configuración ``setup.ini``

.. code-block:: ini

    [PATHS]
    baseDir   = /home/willy/Projects/gsrDB
    dataDir   = ${baseDir}/inData/datos
    schemaDir = ${baseDir}/schema
    # ejemplo de archivo .mdb (sólo para desarrollo)
    oneData   = ${dataDir}/2001-01-10-02_data/Mano Derecha0.mdb

    [MySQL]
    DBname   = gsr0
    username = gsruser
