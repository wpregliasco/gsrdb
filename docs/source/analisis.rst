:orphan:

******************************************************
Estructura de la base de datos
******************************************************

La base de datos está organizada siguiendo la secuencia 
de objetos [#f1]_

.. image:: _static/analisis.svg
   :alt: Desarrollo_2

**File**
   * cada archivo ``.mdb`` genera una entrada en la tabla ``Files``.   
   * hay varias áreas por archivo, pero sólo un scan por archivo y eso ocurre en el Área ``Files::AreaID`` [#f2]_
   * **Tablas:**  :ref:`Files<tablas-file>`
 
**Area**
   * cada portamuestras es un área
   * si hay scans, se organizan en ``Fields``
   * tiene
      * el nombre del stub, 
      * forma y ubicación
      * colección de variables relacionadas con los ``Fields``
   * **Tablas:**   :ref:`Areas<tablas-areas>`,
     :ref:`AreaVariables<tablas-areavariables>` 

**Field**
   * cada *field* es una zona para hacer el scan
   * en cada *field* puedo encontrar varias *features*
   * tenemos las posiciones y un bool si hay datos medidos
   * **Tablas:**   :ref:`Fields<tablas-fields>`

**Feature** 
   * cada *feature* es una partícula a analizar
   * en cada *feature* hay  varios *results* de mediciones
   * cada feature tiene clasificadores: *Rank\_*, *feature_classifications*
   * tiene  
      * *Bounding Box* 
      * valores de *threshold* para la detección
   * **Tablas:**   :ref:`Features<tablas-features>`,
     :ref:`Thresholds<tablas-thresholds>`,
   
**Mediciones** 
   * cada *Result* es una variable medida de un *Feature*
   * el nombre de la medición está en la tabla *Measurements*
   * **Tablas:**   :ref:`Results<tablas-results>`,
     :ref:`Measurements<tablas-measurements>`

**Clasificadores**
   * se aplican a los *feature*
   * ``feature_classifications`` se organiza en ``Classes``
   * ``Features::Rank\_`` señala un campo de la tabla ``Ranks``
   * ``Classes`` es una categorización más amplia que ``Rank\_`` 
     y la incluye explícitamente
   * uno podría construir categorías propias a partir de las mediciones
   *  **Tablas:**   :ref:`feature_classifications<tablas-feature_classifications>`,
      :ref:`Ranks<tablas-ranks>`, :ref:`Classes<tablas-classes>`


.. rubric:: Notas
    
.. [#f1] Usamos algunos términos en inglés para asociarlos más fácilmente con las tablas.
.. [#f2] La sintaxis es ``tabla::columna``