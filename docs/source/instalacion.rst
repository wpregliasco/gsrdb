:orphan:

***********
Instalación
***********

La instalación es larga y aburrida. 

Lo mejor es ser prolijo y hacerlo con cuidado. Una vez bien y adiós.

Dependencias
============

.. code-block:: bash

   $ sudo apt install mdbtools git-all


MySQL
=====

Conviene chequear si no hay una instalación previa, tipeando el comando ``mysql``. 

En caso de no haber respuesta, podemos empezar de cero. 

.. code-block:: bash

   $ sudo apt install mysql-server

Con esto el servicio debería estar corriendo.  Para chequear bastan estos comandos:

.. code-block:: bash

   $ sudo systemctl status mysql
   $ sudo systemctl start  mysql
   $ sudo systemctl enable mysql

Acceso ``root``   
---------------

Para establecer la seguridad del acceso, usar el comando

.. code-block:: bash
   
   $ sudo mysql_secure_installation
   
   
que nos va a hacer una serie de preguntas:

* VALIDATE PASSWORD COMPONENT: N
* ponemos el password del root (*abracadabra*)    
     *El usuario root sólo puede entrar a MySQL con sudo*
* bloqueamos ``anonymous users``
* ``disallow root login remotely``
* responder todo el resto que sí.
   
.. warning::
   
   En cierta ocasión no me anduvo esto. Supongo que era porque tenía cierta parte de la instalación previa.  
   Me bastó con desinstalarlo, reinstalarlo, reboot (No sé por qué necesita el reboot).

Acceso usuario
--------------

La base de datos funciona como un servicio de la máquina.   

Pueden hacer consultas varios procesos y programas, incluso desde otras máquinas.   

El acceso es por *usuario::password* y se pueden limitar las diferentes bases de datos 
y tablas a diferentes usuarios.  

En este ejemplo, creo el usuario *gsruser::bangbang*


.. code-block:: bash

   ###    Acceso a mysql (como root)
   $ sudo mysql -p

   ###    Lista de usuarios
   mysql> SELECT user,authentication_string,plugin,host FROM mysql.user;
   ###    Nuevo usuario
   mysql> CREATE USER 'gsruser'@'localhost' IDENTIFIED BY 'bangbang';
   mysql> GRANT ALL PRIVILEGES ON *.* TO 'gsruser'@'localhost' WITH GRANT OPTION;
   mysql> exit;


Para que no pregunte el password en cada ingreso, editar/crear el archivo ``~/.my.cnf``

.. code-block:: ini

    [mysql]
    user = gsruser
    password = bangbang

    [mysqldump]
    user = gsruser
    password = bangbang

Hacemos ``chmod 600 ~/.my.cnf`` y reiniciamos el servicio con la nueva configuración:

.. code-block:: bash

   sudo systemctl restart mysql


ya podemos usar la base de datos sólo especificando el usuario.

.. warning::
   
   Esta configuración está diseñada para su uso en una máquina personal, 
   que no comparte la base de datos en la red. En otros escenarios es insegura
   y no recomendable.

Verificación
------------

Esta verificación es innecesaria, pero si funciona, es que todo lo anterior está al pelo. 

Entrar a la base de datos con el comando:

.. code-block:: bash

   mysql

debería entrar sin pedir nada más, con el usuario que hemos creado. Lo preguntamos por las dudas:

.. code-block:: mysql
   
   mysql> SELECT CURRENT_USER();
   +-------------------+
   | CURRENT_USER()    |
   +-------------------+
   | gsruser@localhost |
   +-------------------+
   1 row in set (0.00 sec)

   mysql> exit;

Si esto es lo que vemos en la salida, es que todo anduvo bien.

Referencias:
------------

* `Tutorial detallado de instalación <https://www.digitalocean.com/community/tutorials/como-instalar-mysql-en-ubuntu-18-04-es>`_
* `Reset root password <https://support.rackspace.com/how-to/mysql-resetting-a-lost-mysql-root-password/>`_
* `Para desinstalar <https://stackoverflow.com/questions/22909060/mysql-job-failed-to-start>`_


Python
======

La idea es no sólo usar los programas que se proveen en este paquete, 
sino además hacer cierto desarrollo sobre ellos.

En ese caso, recomendamos la instalación via Anaconda que se describe en `la documentación oficial <https://docs.anaconda.com/anaconda/install/>`_.

La **versión corta** consiste en 

* bajar `Anaconda Installer for Linux <https://www.anaconda.com/download/#linux>`_

* instalar:
  
.. code-block:: bash

      # Dependencias
      $ sudo apt install libgl1-mesa-glx libegl1-mesa \
                        libxrandr2 libxrandr2 \ 
                        libxss1 \
                        libxcursor1 libxcomposite1 \
                        libasound2 libxi6 libxtst6

      # Anaconda
      $ bash ~/Downloads/Anaconda2-2019.10-Linux-x86_64.sh


gsrDB
=====
                    
Bajamos el repositorio de ``GitLab``. 

Primero nos ubicamos en el directorio raíz de nuestros proyectos 
y bajamos el paquete con el comando:

.. code-block:: bash

   ~/myProjects$ git clone https://gitlab.com/wpregliasco/gsrdb.git


DbDiver
=======

Environment
-----------

Recomendamos manejar el programa dentro de un _environment_
que llamamos ``gsr``. 

Si el environment no existe
   se pueden instalar todas las dependencias necesarias con el comando

   .. code-block:: bash

      $ conda env create -f environment.yml

Si el environment ya existe, 
   basta activarlo y se pueden actualizar las dependencias

   .. code-block:: bash

      $ conda env update --file environment.yml

Si el environment ya existe, 
   y queremos borrarlo,

   .. code-block:: bash

      $ conda deactivate 
      $ conda env remove --name gsr

.. note::

   la exportación del environment se realiza con el 
   environment activado

   .. code-block:: bash
   
       $ conda env export > environment.yml
   
Módulo Python
-------------

Para que no haya problemas de directorios, podemos registrar el 
módulo de Python ``dbdiver`` para que lo vea desde cualquier directorio
del sistema.

Nos paramos en el directorio principal del proyecto y corremos:

.. code-block:: bash

   $ pip install -e .

y a partir de ahora, podemos importar el módulo en un código python 
haciendo

.. code-block:: python

   import dbdiver


