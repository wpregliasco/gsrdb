.. gsrDB documentation master file, created by
   sphinx-quickstart on Mon Apr 12 14:20:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

######################   
Documentación de gsrDB
######################

Este es un paquete de programas para leer y procesar las bases de 
datos que generan los sistemas automáticos de análisis de GSR que vienen
en los microscopios de barrido. 
(`INCA Energy - Software Interface <https://www.brandonu.ca/microscope/files/2010/08/pdf_3.pdf>`_)

Estos sistemas generan archivos ``.mdb`` que son los que usa *Microsoft Access*. 
Estos archivos son leídos e incorporados a una base de datos *MySQL* desde donde pueden ser
procesadas con una interfaz en Python.

Para un inicio rápido, recomiendo comenzar por:

* :doc:`quickstart`

La documentación está estructurada en tres partes:

* :doc:`instalacion`
* :doc:`transferencia`
* :doc:`modulo` 

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Toda la documentación está hecha para un entorno *Linux (Ubuntu, Debian)* . 
Esto no se debe a que sea la única opción posible, sino a que es 
el entorno que tenemos y estamos familiarizados. 

Referencias
===========

* :doc:`scripts`
* :doc:`analisis`
* :doc:`tablas` 

..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
